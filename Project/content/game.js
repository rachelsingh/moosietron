$( document ).ready( function()
{
    var settings = { width: 640, height: 480, fps: 30 };
    
    $( "canvas" ).attr( "width", settings.width );
    $( "canvas" ).attr( "height", settings.height );
    
    var context = $( "canvas" )[0].getContext( "2d" );
    
    context.fillStyle = "#398eed";
    context.fillRect( 0, 0, settings.width, settings.height );
    
    context.fillStyle = "#ffffff";
    context.fillText( "Loading...", 10, 10 );
    
    // Setup main state
    mainState.Setup( context, settings );
    
    // Add event listeners
    window.addEventListener( "keydown",     mainState.HandleKeyDown, false );
    window.addEventListener( "keyup",       mainState.HandleKeyUp, false );
    //window.addEventListener( "mousedown",   mainState.HandleMouseDown, false );
    
    // Update and draw
    setInterval( function() {
        mainState.Update( settings );
        mainState.Draw( context, settings );
    }, 1000 / settings.fps );
} 
);
