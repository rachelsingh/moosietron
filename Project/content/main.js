mainState = {
	name : "gameState",
	
	tomatoes: [],
	enemies: [],
	bullets: [],
	
	endGameMessage: "",
	gameOver: false,
	
	images: {},
	
	SetupTomatoes: function( settings ) {
		for ( var i = 0; i < 10; i++ ) {
			var tomato = {};
			tomato.x = 0;
			tomato.y = 0;
			tomato.w = 16;
			tomato.h = 16;
			tomato.isAlive = true;
			tomato.image = this.images.tomato;
			this.MoveToRandomPosition( tomato, settings.width, settings.height );
			
			this.tomatoes.push( tomato );
		}
	},
	
	SetupEnemies: function( settings ) {
		for ( var i = 0; i < 3; i++ ) {
			var enemy = {};
			enemy.x = 0;
			enemy.y = 0;
			this.MoveToRandomPosition( enemy, settings.width, settings.height );
			enemy.w = 32;
			enemy.h = 32;
			enemy.speed = 2;
			enemy.image = this.images.enemy;
			enemy.targetTomatoIndex = this.GetRandomTomatoIndex();
			
			this.enemies.push( enemy );
		}
	},
	
	SetupBullet: function( x, y, direction ) {
		var bullet = {};
		bullet.x = x;
		bullet.y = y;
		bullet.w = 16;
		bullet.h = 16;
		bullet.speed = 10;
		bullet.image = this.images.bullet;
		bullet.direction = direction;
		
		this.bullets.push( bullet );
	},

	Setup: function( context, settings ) { 
		this.keys = {
			up: 38, down: 40, left: 37, right: 39,
			w: 87, s: 83, a: 65, d: 68,
		};
		
		this.score = 0;
		
		this.images.player = new Image();
		this.images.player.src = "content/player.png";
		this.images.enemy = new Image();
		this.images.enemy.src = "content/enemy1.png";
		this.images.tomato = new Image();
		this.images.tomato.src = "content/tomato.png";
		this.images.bullet = new Image();
		this.images.bullet.src = "content/bullet.png";
		
		this.player = {}
		this.player.keyPress = { up: false, down: false, left: false, right: false };
		this.player.x = settings.width/2 - 16;
		this.player.y = settings.height/2 - 16;
		this.player.w = 32;
		this.player.h = 32;
		this.player.speed = 5;
		this.player.bulletCooldown = 0;
		this.player.image = this.images.player;
		
		this.SetupTomatoes( settings );
		this.SetupEnemies( settings );
	},
	
	GetRandomTomatoIndex: function() {
		return Math.floor( Math.random() * this.tomatoes.length );
	},
	
	HandleKeyUp: function( event ) { 
		if ( event.keyCode == mainState.keys.up ) {
			mainState.player.keyPress.up = false;
		}
		else if ( event.keyCode == mainState.keys.down ) {
			mainState.player.keyPress.down = false;
		}
		else if ( event.keyCode == mainState.keys.left ) {
			mainState.player.keyPress.left = false;
		}
		else if ( event.keyCode == mainState.keys.right ) {
			mainState.player.keyPress.right = false;
		}
	},

	HandleKeyDown: function( event ) { 
		if ( event.keyCode == mainState.keys.up ) {
			mainState.player.keyPress.up = true;
		}
		else if ( event.keyCode == mainState.keys.down ) {
			mainState.player.keyPress.down = true;
		}
		if ( event.keyCode == mainState.keys.left ) {
			mainState.player.keyPress.left = true;
		}
		else if ( event.keyCode == mainState.keys.right ) {
			mainState.player.keyPress.right = true;
		}
		
		// Shooting
		if ( mainState.player.bulletCooldown == 0 ) {
			var keysPressed = { up:false, down:false, left: false, right: false };
			var isShooting = true;
			
			if ( event.keyCode == mainState.keys.w ) {
				mainState.SetupBullet( mainState.player.x, mainState.player.y, "up" );
			}
			else if ( event.keyCode == mainState.keys.s ) {
				mainState.SetupBullet( mainState.player.x, mainState.player.y, "down" );
			}
			else if ( event.keyCode == mainState.keys.a ) {
				mainState.SetupBullet( mainState.player.x, mainState.player.y, "left" );
			}
			else if ( event.keyCode == mainState.keys.d ) {
				mainState.SetupBullet( mainState.player.x, mainState.player.y, "right" );
			}
			else { 
				isShooting = false;
			}
			
			if ( isShooting ) {
				mainState.player.bulletCooldown = 10;
			}
		}
	},
	
	MoveToRandomPosition( tomato, width, height ) {
		tomato.x = Math.random() * width;
		tomato.y = Math.random() * height;
	},
	
	UpdateBullets: function() {
		var removeBullets = [];
		var removeTomatoes = [];
		var removeEnemies = [];
		
		for ( var i = 0; i < this.bullets.length; i++ ) {
			if ( this.bullets[i].direction == "up" ) {
				this.bullets[i].y -= this.bullets[i].speed;
			}
			else if ( this.bullets[i].direction == "down" ) {
				this.bullets[i].y += this.bullets[i].speed;
			}
			else if ( this.bullets[i].direction == "left" ) {
				this.bullets[i].x -= this.bullets[i].speed;
			}
			else if ( this.bullets[i].direction == "right" ) {
				this.bullets[i].x += this.bullets[i].speed;
			}
			
			// Collision with enemies?
			for ( var e = 0; e < this.enemies.length; e++ ) {
				if ( this.IsCollision( this.bullets[i], this.enemies[e] ) ) {
					removeEnemies.push( e );
					removeBullets.push( i );
					this.score += 5;
				}
			}
			
			// Collision with tomatoes?
			for ( var t = 0; t < this.tomatoes.length; t++ ) {
				if ( this.IsCollision( this.bullets[i], this.tomatoes[t] ) ) {
					removeTomatoes.push( t );
					removeBullets.push( i );
					this.score -= 15;
				}
			}
		}
		
		for ( var i = 0; i < removeBullets.length; i++ )
		{
			this.bullets.splice( removeBullets[i], 1 );
		}
		for ( var i = 0; i < removeTomatoes.length; i++ )
		{
			this.tomatoes.splice( removeTomatoes[i], 1 );
		}
		for ( var i = 0; i < removeEnemies.length; i++ )
		{
			this.enemies.splice( removeEnemies[i], 1 );
		}
	},
	
	UpdatePlayer: function() {
		if ( this.player.keyPress.up ) {
			this.player.y -= this.player.speed;
		}
		else if ( this.player.keyPress.down ) {
			this.player.y += this.player.speed;
		}
		if ( this.player.keyPress.left ) {
			this.player.x -= this.player.speed;
		}
		else if ( this.player.keyPress.right ) {
			this.player.x += this.player.speed;
		}
		
		if ( this.player.bulletCooldown > 0 ) {
			this.player.bulletCooldown -= 1;
		}
	},
	
	UpdateEnemies: function() {
		if ( this.tomatoes.length == 0 ) { return; }
		
		for ( var i = 0; i < this.enemies.length; i++ ) {
			if ( this.IsCollision( this.enemies[i], this.player ) ) {
				this.EndGame( "Game Over - The moose killed you!" );
			}
			
			var target = this.tomatoes[ this.enemies[i].targetTomatoIndex ];
			if ( target == undefined ) {
				this.enemies[i].targetTomatoIndex = this.GetRandomTomatoIndex();
			}
			
			if ( target.x < this.enemies[i].x ) { this.enemies[i].x -= this.enemies[i].speed; }
			if ( target.x > this.enemies[i].x ) { this.enemies[i].x += this.enemies[i].speed; }
			if ( target.y < this.enemies[i].y ) { this.enemies[i].y -= this.enemies[i].speed; }
			if ( target.y > this.enemies[i].y ) { this.enemies[i].y += this.enemies[i].speed; }
		}
	},
	
	UpdateTomatoes: function() {
		var removeIndices = [];
		for ( var i = 0; i < this.tomatoes.length; i++ ) {
			
			for ( var e = 0; e < this.enemies.length; e++ ) {
				if ( this.IsCollision( this.enemies[e], this.tomatoes[i] ) && this.tomatoes[i].isAlive ) {
					this.tomatoes[i].isAlive = false;
					removeIndices.push( i );
				}
			}
			
			if ( this.IsCollision( this.player, this.tomatoes[i] ) && this.tomatoes[i].isAlive ) {
				this.score += 10;
				this.tomatoes[i].isAlive = false;
				removeIndices.push( i );
			}
		}
		
		for ( var i = 0; i < removeIndices.length; i++ )
		{
			this.tomatoes.splice( removeIndices[i], 1 );
		}
	},
	
	IsCollision: function( obj1, obj2 ) {
		return ( 	obj1.x < obj2.x + obj2.w &&
					obj1.x + obj1.w > obj2.x &&
					obj1.y < obj2.y + obj2.h &&
					obj1.y + obj1.h > obj2.y );
	},
	
	EndGame: function( message ) {
		this.endGameMessage = message;
		this.gameOver = true;
	},

	Update: function( settings ) {
		if ( this.gameOver ) { return; }
		 
		this.UpdateEnemies();
		this.UpdateTomatoes();
		this.UpdatePlayer();
		this.UpdateBullets();
		
		if ( this.enemies.length == 0 ) {
			if ( this.score <= 0 ) {
				this.EndGame( "Game Over - You failed to protect the tomatoes!" );
			}
			else {
				this.EndGame( "You win!" );
			}
		}
	},

	Draw: function( context, settings ) { 
		context.fillStyle = "#000000";
		context.fillRect( 0, 0, settings.width, settings.height );
		
		context.drawImage( this.player.image, this.player.x, this.player.y );
		
		for ( var i = 0; i < this.enemies.length; i++ ) {
			context.drawImage( this.enemies[i].image, this.enemies[i].x, this.enemies[i].y );
		}
		
		for ( var i = 0; i < this.tomatoes.length; i++ ) {
			if ( this.tomatoes[i].isAlive ) {
				context.drawImage( this.tomatoes[i].image, this.tomatoes[i].x, this.tomatoes[i].y );
			}
		}
		
		for ( var i = 0; i < this.bullets.length; i++ ) {
			context.drawImage( this.bullets[i].image, this.bullets[i].x, this.bullets[i].y );
		}
		
		context.fillStyle = "#ffffff";
		context.fillText( "Score: " + this.score, 10, 10 );
		
		if ( this.endGameMessage != "" ) {
			context.fillText( this.endGameMessage, settings.width / 2, settings.height / 2 );
		}
	}
}
